﻿/******************************************************************************/
#include "stdafx.h"
#include "@@headers.h"
/******************************************************************************/
Mesh box        , // mesh box
     ball       ; // mesh ball
Vec  ball_pos[8]; // ball positions
flt  mouse_yaw  ; // camera yaw from mouse
bool initialized; // if settings were initialized according to oculus rift
/******************************************************************************/
void InitPre()
{
   EE_INIT();
   App.flag=APP_MS_EXCLUSIVE|APP_FULL_TOGGLE;

   Gui.stereoscopic_scale =0.6f;
   Gui.stereoscopic_offset=0.03f;
}
/******************************************************************************/
bool Init()
{
   // create meshes
   box  .parts.New().base.create(Box (1   ), VTX_TEX0|VTX_NRM|VTX_TAN).reverse(); // create mesh box, reverse it because it's meant to be viewed from inside
   ball .parts.New().base.create(Ball(0.15f), VTX_TEX0|VTX_NRM|VTX_TAN); // create mesh ball

   // set mesh materials, rendering versions and bounding boxes
   box .material(UID(2123216029, 1141820639, 615850919, 3316401700)).setRender().setBox();
   ball.material(UID(2123216029, 1141820639, 615850919, 3316401700)).setRender().setBox();

   // set random positions
   REPAO(ball_pos)=Random(Box(0.9f));

   return true;
}
/******************************************************************************/
void Shut()
{
}
/******************************************************************************/
bool Update()
{
   if(Kb.bp(KB_ESC))return false;

   D.stereoscopic(OculusRiftDetected()); // set stereoscopic rendering if Oculus Rift is detected
   if(!initialized && OculusRiftDetected())
   {
      initialized=true;
      D.mode(OculusRiftScreenRes().x, OculusRiftScreenRes().y, 0); // set recommended Resolution
      D.eyeDistance(OculusRiftEyeDistance()); // set recommended Eye Distance
      D.viewFov(OculusRiftFov()); // set recommended Field of View
      D.density(OculusRiftPixelDensity()); // set recommended pixel density
   }

   if(Kb.bp(KB_R))OculusRiftRecenter();

   // set camera based on Oculus Rift input
   mouse_yaw-=Ms.d().x;
   Vec angles=OculusRiftPose().angles();
   Cam.at   =OculusRiftPose().pos;
   Cam.yaw  =-angles.y+mouse_yaw;
   Cam.pitch=-angles.x;
   Cam.roll =-angles.z;
   Cam.dist =0;
   Cam.setSpherical().updateVelocities().set();

   return true;
}
/******************************************************************************/
void Render()
{
   switch(Renderer())
   {
      case RM_PREPARE:
      {
         // solid objects
                       box .draw(MatrixIdentity);
         REPA(ball_pos)ball.draw(Matrix(ball_pos[i]));
         LightSqr(2, Vec(0, 0.8f, 0)).add();
      }break;

      case RM_SHADOW:
      {
         REPA(ball_pos)ball.drawShadow(Matrix(ball_pos[i]));
      }break;
   }
}
void Draw()
{
   Renderer(Render);
   if(OculusRiftDetected())
   {
      D.text(0, 0.1f, L"Press 'R' to recenter Oculus Rift");
   }else
   {
      D.text(0, 0, L"Oculus Rift not detected");
   }
}
/******************************************************************************/

/******************************************************************************/
